<?php 

/*
  Stored form values for each step  
  $form_state['storage']['form_wizard']['values'][$step] 
  
  $form_state['storage']['form_wizard']['step']
  
*/

function form_wizard_form(&$form_state, $wizard) { 
  $form['#wizard'] = $wizard; 
  if (empty($form_state['storage']['form_wizard']['step'])) { 
    $form['#wizard_step'] = _form_wizard_first($wizard); 
    $form['#wizard_form']  = $wizard[$form['#wizard_step']]; 
    
  }
  else { 
    $form['#wizard_step'] = $form_state['storage']['form_wizard']['step'];
    $form['#wizard_form'] = $wizard[$form['#wizard_step'] ];
  }
  $current_step = $form['#wizard_step']; 

  if (!is_null(form_wizard_prev($wizard, $current_step))) {
    $form['prev'] = array(
      '#type' => 'submit',
      '#value' => t('<< Prev'),
      '#submit' => array ('form_wizard_prev_submit')
    );
  }    
  $next = form_wizard_next($wizard, $current_step); 
  if (!is_null($next)) { 
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next >>'),
      '#submit' => array ('form_wizard_next_submit')
    );  
  }
  
  if ($wizard[$current_step]['#allow_finish'] || is_null ($next) ) { 
    $form['finish'] = array(
      '#type' => 'submit',
      '#value' => t('Finish'),
      '#submit' => array ('form_wizard_finish_submit')
    );     
  }
    
  $form['#submit'] = array ( 'form_wizard_submit'); 
  
  if (empty($form_state['storage']['form_wizard']['values'][$current_step])) {
    $step_form_values = NULL;
  }
  else {
    $step_form_values = $form_state['storage']['form_wizard']['values'][$current_step];
  }
  
  $form_function = $wizard[$current_step]['#form']; 
  $form_parameters = $wizard[$current_step]['#parameters']; 
  array_push ($form_parameters, $step_form_values) ;  
  if ($wizard[$current_step]['#form_state'])
    array_unshift($form_parameters, $form_state);
  return array_merge (call_user_func_array($form_function, $form_parameters), $form); 
}

function form_wizard ($wizard) {
  $wizard_defaults = array(
    '#description' => NULL,
  );
  $form_defaults = array(
    '#parameters' => array(), 
    '#allow_finish' => FALSE, 
    '#ahah' => TRUE, 
    '#form_state' => FALSE
  );    
  $wizard = array_merge_recursive($wizard, $wizard_defaults);
  foreach ($wizard as $step => $form) {
    if (!element_property($step)) {
      $wizard[$step] = array_merge_recursive($form_defaults, $form);
    }
  }
  
  return drupal_get_form('form_wizard_form', $wizard); 
}

function form_wizard_next_submit($form, &$form_state) {
  $form_state['storage']['form_wizard']['previous_step'] = $form['#wizard_step'];
  $form_state['storage']['form_wizard']['step'] = form_wizard_next($form['#wizard'], $form['#wizard_step']);
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
}

function form_wizard_prev_submit($form, &$form_state) {
  $form_state['storage']['form_wizard']['previous_step'] = $form['#wizard_step'];
  $form_state['storage']['form_wizard']['step'] = form_wizard_prev($form['#wizard'], $form['#wizard_step']);
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);
}

function form_wizard_finish_submit($form, &$form_state) {
  $form_state['storage']['form_wizard']['values'][$form['#wizard_step']] = $form_state['values']; 
  $form['#submit'] = array_merge($form['#submit'], $form['#wizard']['#finish']); 
  unset($form_state['submit_handlers']);
  form_execute_handlers('submit', $form, $form_state);  
  unset($form_state['storage']);
  unset($form_state['rebuild']); 
}

function form_wizard_submit($form, &$form_state) {
  // Save step form data
  $form_state['storage']['form_wizard']['values'][$form_state['storage']['form_wizard']['previous_step']] = $form_state['values']; 
  drupal_set_message ('form submit handler executed') ; 
}

function wizard () {
  $wizard = array(
    '#finish' => array ('test_finish'), 
    '#title' => t('Wizard example'), 
    
    // steps declarations as array elements
  
    'website_url' => array (
      '#tilte' => t('Website'),
      '#description' => t('In this step define your website URL'),  
      '#form' => 'form1', 
      '#form_state' => TRUE,
      '#ahah' => TRUE,
    ), 
  
    'user_info' => array (
      '#tilte' => t('Personal information'),
      '#description' => t('Please, specify your personal information. This information will not be shared.'),  
      '#form' => 'form2', 
      '#ahah' => FALSE, 
      '#allow_finish' => TRUE
    ),  
  
    'aditional' => array (
      '#tilte' => t('Aditional information'),
      '#description' => t('Aditional information will b helpful for us'),  
      '#form' => 'form3', 
      '#ahah' => TRUE, 
     )
  );
  return $wizard;
}

function form_wizard_next ($wizard, $step) {
  $steps = array_values (element_children($wizard)); 
  $next = array_search($step, $steps) + 1;
  return isset ($steps[$next]) ? $steps[$next] : NULL;
}

function form_wizard_prev($wizard, $step) {
  $steps = array_values (element_children($wizard)); 
  $prev = array_search($step, $steps) - 1;
  return isset ($steps[$prev]) ? $steps[$prev] : NULL;
}

function _form_wizard_first ($wizard) {
  return reset (element_children($wizard));
}
function form1(&$form_state, $form_values) { 
  $form['website_url'] = array (
    '#type' => 'textfield', 
    '#title' => t ('URL'), 
    '#description' => t ('Enter the URL of your website. Example: http://www.example.com'), 
    '#default_value' => isset( $form_values['website_url']) ? $form_values['website_url'] : '', 
    '#required' => true
  ); 
  return $form; 
}

function form2($form_values) { 
  $form['username'] = array (
    '#type' => 'textfield', 
    '#title' => t ('Your name'), 
    '#description' => t ('Enter your name'), 
    '#default_value' => isset( $form_values['username']) ? $form_values['username'] : '',
     '#required' => true
  ); 
  return $form; 
}

function form3($form_values) { 
  $form['aditional'] = array (
    '#type' => 'textarea', 
    '#title' => t ('Aditional info'), 
    '#description' => t ('Aditional info'), 
    '#default_value' => isset( $form_values['aditional']) ? $form_values['aditional'] : ''
  ); 
  return $form; 
}

function test_finish ($form, &$form_state) { 
  drupal_set_message ('finish handler'); 
  $form_state['redirect'] = "admin";
}